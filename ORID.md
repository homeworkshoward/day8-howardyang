O(Objective):
+ Learned the three-layer model of spring boot,  which includes Controller layer, Service layer, and Repository layer.
+ Learned the Integration testing development of Spring Boot, which requests instructions through code rather than software.
+ I learned unit testing and simulated the output through Mock.

R(Reflective):
+ Fruitful

I(Interpretive):
+ I am not very clear about the operation of three-layer splitting.
+ I am not very proficient in using Mock to simulate output operations.

D(Decision):
+ Continue practicing the three-layer structure of Spring Boot. 
+ Continue to familiarize and practice unit testing and Integration testing.
