package com.afs.restapi.Service;

import com.afs.restapi.exception.EmployCreatedException;
import com.afs.restapi.exception.UnemployedException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class EmployeeServiceTest {
    private final EmployeeRepository employeeRepository = mock(EmployeeRepository.class);
    private final EmployeeService employeeService = new EmployeeService(employeeRepository);

    @ParameterizedTest
    @ValueSource(ints = {18, 65})
    void should_return_created_employee_when_create_employee_given_employee_age_is_between_18_to_65(Integer age) throws EmployCreatedException {

        Employee employee = new Employee(null, "Lily", age, "female", 20000);
        when(employeeRepository.insert(eq(employee)))
                .thenReturn(new Employee(1L, "Lily", age, "female", 20000));
        Employee employeeResponse = employeeService.create(employee);

        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
    }


    @ParameterizedTest
    @ValueSource(ints = {17, 66})
    void should_throw_exception_when_create_employee_given_employee_age_is_not_between_18_to_65(Integer age) {

        Employee employee = new Employee(null, "Lily", age, "female", 20000);

        EmployCreatedException employeeCreatedException = assertThrows(EmployCreatedException.class, () -> employeeService.create(employee));

        assertEquals("Employee must be 18-65 years old.", employeeCreatedException.getMessage());

    }

    @ParameterizedTest(name = "{index} employee age is {0} and salary is {1}")
    @CsvSource({"30,20000", "29,19999"})
    void should_return_created_employee_when_create_employee_given_(Integer age, Integer salary) throws EmployCreatedException {
        Employee employee = new Employee(null, "Lily", age, "female", salary);
        when(employeeRepository.insert(employee))
                .thenReturn(new Employee(1L, "Lily", age, "female", salary));

        Employee employeeResponse = employeeService.create(employee);
        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(age, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(salary, employeeResponse.getSalary());

    }

    @Test
    void should_throw_exception_when_create_employee_given_age_is_30_and_salary_is_19999() {
        Employee employee = new Employee(null, "Lily", 30, "female", 19999);
        EmployCreatedException employCreatedException = assertThrows(
                EmployCreatedException.class,
                () -> employeeService.create(employee));
        assertEquals("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created.", employCreatedException.getMessage());
    }

    @Test
    void should_set_active_when_create_employee() throws EmployCreatedException {
        Employee employee = new Employee(null, "Lily", 30, "female", 20000);
        when(employeeRepository.insert(employee))
                .thenReturn(new Employee(1L, "Lily", 30, "female", 20000, true));
        Employee employeeResponse = employeeService.create(employee);
        assertNotNull(employeeResponse);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(30, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
        assertTrue(employeeResponse.getActive());
    }

    @Test
    void should_set_active_false_when_delete_employee() throws EmployCreatedException {
        Employee employee = new Employee(1L, "Lily", 30, "female", 20000);
        when(employeeRepository.findById(employee.getId()))
                .thenReturn(employee);
        Employee employeeResponse = employeeService.delete(1L);
        assertEquals(1L, employeeResponse.getId());
        assertEquals("Lily", employeeResponse.getName());
        assertEquals(30, employeeResponse.getAge());
        assertEquals("female", employeeResponse.getGender());
        assertEquals(20000, employeeResponse.getSalary());
        assertFalse(employeeResponse.getActive());
    }

    @Test
    void should_update_employee_only_when_be_employed() throws UnemployedException {

        Employee employeeEmployed = new Employee(1L, "Lily", 30, "female", 20000, 1L);

        when(employeeRepository.findById(employeeEmployed.getId()))
                .thenReturn(employeeEmployed);

        when(employeeRepository.update(employeeEmployed.getId(), new Employee(31, 21000)))
                .thenReturn(new Employee(1L, "Lily", 31, "female", 21000, 1L));

        Employee employeeEmployedResponse = employeeService.update(1L, new Employee(31, 21000));
        assertEquals(1L, employeeEmployedResponse.getId());
        assertEquals("Lily", employeeEmployedResponse.getName());
        assertEquals(31, employeeEmployedResponse.getAge());
        assertEquals("female", employeeEmployedResponse.getGender());
        assertEquals(21000, employeeEmployedResponse.getSalary());
    }

    @Test
    void should_throw_error_when_update_the_unemployed() {
        Employee employeeUnemployed = new Employee(1L, "Lily", 30, "female", 20000);
        when(employeeRepository.findById(employeeUnemployed.getId()))
                .thenReturn(employeeUnemployed);
        when(employeeRepository.update(employeeUnemployed.getId(), new Employee(31, 21000)))
                .thenReturn(new Employee(1L, "Lily", 31, "female", 21000, 1L));

        UnemployedException unemployedException = assertThrows(UnemployedException.class,
                () -> employeeService.update(1L, new Employee(31, 21000)));
        assertEquals("It can't be update.", unemployedException.getMessage());
    }


}
