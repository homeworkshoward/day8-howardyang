package com.afs.restapi;

import com.afs.restapi.Service.CompanyService;
import com.afs.restapi.Service.EmployeeService;
import com.afs.restapi.exception.EmployCreatedException;
import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class EmployeeIntegrationTest {

    @Autowired
    MockMvc mockMvc;
    MockMvc client;
    ObjectMapper mapper = new ObjectMapper();
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    EmployeeService employeeService;
    @Autowired
    CompanyService companyService;
    @BeforeEach
    void setUp() {
        employeeRepository.clearAll();
    }

    @Test
    void should_get_all_employees_when_perform_get_given_employees() throws Exception {
        //given

        employeeService.create(new Employee(1L,"Susan", 22, "Female", 10000));

        //when
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andReturn();

        //then
        MockHttpServletResponse response = mvcResult.getResponse();
        assertEquals(200, response.getStatus());
        List<Employee> employees = mapper.readValue(response.getContentAsString(), new TypeReference<List<Employee>>() {
        });
        assertEquals(1, employees.size());
        Employee employeeGet = employees.get(0);
        assertEquals(1, employeeGet.getId());
        assertEquals("Susan", employeeGet.getName());
        assertEquals(22, employeeGet.getAge());
        assertEquals("Female", employeeGet.getGender());
        assertEquals(10000, employeeGet.getSalary());
    }

    private static Employee buildEmployeeSusan() {
        Employee newEmployee = new Employee(1L, "Susan", 22, "Female", 10000,true);
        return newEmployee;
    }

    @Test
    void should_get_all_employees_when_perform_get_given_employees_use_matchers() throws Exception {
        //given
        Employee newEmployee = buildEmployeeSusan();
        employeeService.create(newEmployee);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000));
    }

    @Test
    void should_return_employee_susan_with_id_1_when_perform_get_by_id_given_employees_in_repo() throws Exception {
        //given
        Employee employee1 = buildEmployeeSusan();
        Employee employee2 = buildEmployeeLisi();
        employeeService.create(employee1);
        employeeService.create(employee2);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").isString())
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").isNumber())
                .andExpect(jsonPath("$.salary").value(10000));
    }

    private static Employee buildEmployeeLisi() {
        return new Employee(2L, "Lisi", 30, "Man", 20000);
    }

    @Test
    void should_return_employees_by_params_when_perform_get_by_params_given_employees_in_repo() throws Exception {
        //given
        Employee employee1 = buildEmployeeSusan();
        Employee employee2 = buildEmployeeLisi();
        Employee employee3 = buildEmployeeWangwu();
        employeeService.create(employee1);
        employeeService.create(employee2);
        employeeService.create(employee3);
        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees?gender=Man"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(2))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Lisi"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(30))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Man"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(20000))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(3))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("WangWu"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(30))
                .andExpect(jsonPath("$[1].gender").isString())
                .andExpect(jsonPath("$[1].gender").value("Man"))
                .andExpect(jsonPath("$[1].salary").isNumber())
                .andExpect(jsonPath("$[1].salary").value(20000));
    }

    private static Employee buildEmployeeWangwu() {
        return new Employee(3L, "WangWu", 30, "Man", 20000);
    }


    @Test
    void should_return_one_page_employees_when_perform_get_by_page_given_employees_in_repo() throws Exception {

        //given
        Employee employee1 = buildEmployeeSusan();
        Employee employee2 = buildEmployeeLisi();
        Employee employee3 = buildEmployeeWangwu();
        employeeService.create(employee1);
        employeeService.create(employee2);
        employeeService.create(employee3);

        //when
        mockMvc.perform(MockMvcRequestBuilders.get("/employees?pageNumber=1&pageSize=2"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").isNumber())
                .andExpect(jsonPath("$[0].id").value(1))
                .andExpect(jsonPath("$[0].name").isString())
                .andExpect(jsonPath("$[0].name").value("Susan"))
                .andExpect(jsonPath("$[0].age").isNumber())
                .andExpect(jsonPath("$[0].age").value(22))
                .andExpect(jsonPath("$[0].gender").isString())
                .andExpect(jsonPath("$[0].gender").value("Female"))
                .andExpect(jsonPath("$[0].salary").isNumber())
                .andExpect(jsonPath("$[0].salary").value(10000))
                .andExpect(jsonPath("$[1].id").isNumber())
                .andExpect(jsonPath("$[1].id").value(2))
                .andExpect(jsonPath("$[1].name").isString())
                .andExpect(jsonPath("$[1].name").value("Lisi"))
                .andExpect(jsonPath("$[1].age").isNumber())
                .andExpect(jsonPath("$[1].age").value(30))
                .andExpect(jsonPath("$[1].gender").isString())
                .andExpect(jsonPath("$[1].gender").value("Man"))
                .andExpect(jsonPath("$[1].salary").isNumber())
                .andExpect(jsonPath("$[1].salary").value(20000));
    }

    @Test
    void should_return_employee_save_with_id_when_perform_post_given_a_employee() throws Exception {
        //given
        Employee employeeSusan = buildEmployeeSusan();
        String susanJson = mapper.writeValueAsString(employeeSusan);

        //when
        mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").isString())
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").isNumber())
                .andExpect(jsonPath("$.salary").value(10000));
        Employee employeeSaved = employeeService.getEmployeeById(1L);
        assertEquals(employeeSusan.getId(), employeeSaved.getId());
        assertEquals(employeeSusan.getAge(), employeeSaved.getAge());
        assertEquals(employeeSusan.getGender(), employeeSaved.getGender());
        assertEquals(employeeSusan.getName(), employeeSaved.getName());
        assertEquals(employeeSusan.getSalary(), employeeSaved.getSalary());
    }

    @Test
    void should_update_employee_in_repo_when_perform_put_by_id_given_employee_in_repo_and_update_info() throws Exception {

        //given
        Employee employeeSusan = buildEmployeeSusan();
        employeeSusan.setCompanyId(1L);
        employeeService.create(employeeSusan);

        Employee toBeUpdateSusan = buildEmployeeSusan();

        toBeUpdateSusan.setSalary(20000);
        employeeSusan.setCompanyId(1L);
        String susanJson = mapper.writeValueAsString(toBeUpdateSusan);

        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/employees/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(susanJson))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").isNumber())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.name").isString())
                .andExpect(jsonPath("$.name").value("Susan"))
                .andExpect(jsonPath("$.age").isNumber())
                .andExpect(jsonPath("$.age").value(22))
                .andExpect(jsonPath("$.gender").isString())
                .andExpect(jsonPath("$.gender").value("Female"))
                .andExpect(jsonPath("$.salary").isNumber())
                .andExpect(jsonPath("$.salary").value(20000));
        Employee suSanInRepo = employeeService.getEmployeeById(1L);
        assertEquals(toBeUpdateSusan.getSalary(), suSanInRepo.getSalary());
    }

    @Test
    void should_get_all_employees_when_get_all_employees() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        employeeService.create(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].salary").value(5000));
    }

    @Test
    void should_return_employee_when_get_employee_by_id_given_id() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        Employee savedEmployee = employeeRepository.insert(employee);
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees/{id}", savedEmployee.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
    }

    @Test
    void should_create_employee_when_create_employee_given_employee() throws Exception {
        //given
        Employee employee = new Employee(1L, "Lucy", 20, "female", 5000);
        String employeeJson = "{\n" +
                "\"name\": \"Lucy\",\n" +
                "\"age\": 20,\n" +
                "\"gender\": \"female\",\n" +
                "\"salary\": 5000\n" +
                "}";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.post("/employees")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(employeeJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNumber())
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(20))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Lucy"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.gender").value("female"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(5000));
    }

    @Test
    void should_set_active_false_when_delete_the_employee_given_id() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy1", 20, "female", 5000);
        Employee employee2 = new Employee(2L, "Lucy2", 20, "female", 5000);
        employeeService.create(employee1);
        employeeService.create(employee2);
        Long idToBeDeleted = employee2.getId();
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.delete("/employees/{id}", idToBeDeleted))
                .andExpect(MockMvcResultMatchers.jsonPath("$.active").value(false));
    }

    @Test
    void should_return_employees_list_when_findEmployeesByGender_given_gender() throws Exception {
        //given
        Employee employee1 = new Employee(1L, "Lucy1", 20, "female", 5000);
        Employee employee2 = new Employee(2L, "Lucy2", 20, "female", 5000);
        Employee employee3 = new Employee(3L, "Lucy3", 20, "male", 5000);
        employeeRepository.insert(employee1);
        employeeRepository.insert(employee2);
        employeeRepository.insert(employee3);
        String targetGender = "female";
        //when
        //then
        mockMvc.perform(MockMvcRequestBuilders.get("/employees").param("gender", targetGender))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$",hasSize(2)));
    }

    @Test
    void should_return_update_company_when_update() throws Exception {
        companyService.create(new Company());

        String requestJson = mapper.writeValueAsString(new Company(1L,"name"));
        mockMvc.perform(MockMvcRequestBuilders.put("/companies/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
                .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("name"));
    }

    @Test
    void should_return_update_employee_when_update() throws Exception {
        employeeService.create(new Employee(1L,"Susan", 22, "Female", 10000,1L));
        Employee employee = new Employee(23, 200000);
        String requestJson = mapper.writeValueAsString(employee);
        //when
        mockMvc.perform(MockMvcRequestBuilders.put("/employees/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(MockMvcResultMatchers.jsonPath("$.age").value(23))
                .andExpect(MockMvcResultMatchers.jsonPath("$.salary").value(200000));
    }


}
