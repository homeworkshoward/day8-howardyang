package com.afs.restapi.controller;

import com.afs.restapi.Service.EmployeeService;
import com.afs.restapi.model.Employee;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {
    @Autowired
    private MockMvc client;
    @MockBean
    private EmployeeService employeeService;
    @Autowired
    private ObjectMapper mapper;
    @Test
    void  should_call_service_when_perform_post_employee_given_employee() throws Exception {
        Employee employee = new Employee(null,"Tom",20, "Male",2000);
        String requestJson = mapper.writeValueAsString(employee);
        client.perform(post("/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(requestJson))
                .andExpect(status().isCreated());
        verify(employeeService).create(employee);
    }


}
