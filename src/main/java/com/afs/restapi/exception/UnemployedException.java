package com.afs.restapi.exception;

import org.apache.logging.log4j.message.Message;

public class UnemployedException extends Exception{
    public UnemployedException(String message) {
        super(message);
    }
}
