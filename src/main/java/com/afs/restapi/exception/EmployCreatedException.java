package com.afs.restapi.exception;

public class EmployCreatedException extends Exception{
    public EmployCreatedException(String message) {
        super(message);
    }
}
