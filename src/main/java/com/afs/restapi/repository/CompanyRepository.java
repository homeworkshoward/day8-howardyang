package com.afs.restapi.repository;

import com.afs.restapi.exception.NotFoundException;
import com.afs.restapi.model.Company;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    private static final List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
        companies.add(new Company(1L, "spring"));
        companies.add(new Company(2L, "boot"));
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public Company findById(Long id) {
        return getCompanies().stream()
                .filter(company -> company.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return getCompanies().stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company addCompany(Company company) {
        company.setId(generateNewId());
        companies.add(company);
        return company;
    }

    public Company update(Long id, Company company) {
        return getCompanies().stream()
                .filter(storedCompany -> storedCompany.getId().equals(id))
                .findFirst()
                .map(storedCompany -> updateCompanyAttributes(storedCompany, company))
                .orElse(null);
    }

    public void delete(Long id) {
        getCompanies().removeIf(company -> Objects.equals(company.getId(), id));
    }

    private Company updateCompanyAttributes(Company companyStored, Company company) {
        if (company.getName() != null) {
            companyStored.setName(company.getName());
        }
        return companyStored;
    }

    private Long generateNewId() {
        return companies.stream()
                .mapToLong(Company::getId)
                .max()
                .orElse(0L) + 1;
    }
}

