package com.afs.restapi.Service;

import com.afs.restapi.model.Company;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public Company create(Company company) {
        return companyRepository.addCompany(company);
    }


    public List<Company> getCompanies() {
        return companyRepository.getCompanies();
    }

    public Company getCompanyById(Long id) {
        return companyRepository.findById(id);
    }

    public List<Company> getCompaniesByPagination(Integer page, Integer size) {
        return companyRepository.findByPage(page, size);
    }

    public Company update(Long id, Company company) {
        return companyRepository.update(id, company);
    }

    public void remove(Long id) {
        companyRepository.delete(id);
        employeeRepository.findAll().stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(), id))
                .forEach(employee -> employee.setCompanyId(null));
    }
}
