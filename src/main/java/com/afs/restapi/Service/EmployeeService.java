package com.afs.restapi.Service;

import com.afs.restapi.exception.EmployCreatedException;
import com.afs.restapi.exception.UnemployedException;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee create(Employee employee) throws EmployCreatedException {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new EmployCreatedException("Employee must be 18-65 years old.");
        }
        if (employee.getSalary() < 20000 && employee.getAge() >= 30) {
            throw new EmployCreatedException("Employees over 30 years of age (inclusive) with a salary below 20000 cannot be created.");
        }
        employee.setActive(true);
        return employeeRepository.insert(employee);
    }

    public Employee delete(Long employeeId) {
        Employee employee = employeeRepository.findById(employeeId);
        employee.setActive(false);
        return employee;

    }

    public Employee update(Long employeeId, Employee employee) throws UnemployedException {
        if (!Objects.isNull(employeeRepository.findById(employeeId).getCompanyId())) {
            return employeeRepository.update(employeeId, employee);
        } else {
            throw new UnemployedException("It can't be update.");
        }
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findEmployeesByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public List<Employee> findByCompanyId(Long id) {
        return employeeRepository.findByCompanyId(id);
    }
}
