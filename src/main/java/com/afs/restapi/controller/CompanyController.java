package com.afs.restapi.controller;


import com.afs.restapi.Service.CompanyService;
import com.afs.restapi.Service.EmployeeService;
import com.afs.restapi.model.Company;
import com.afs.restapi.model.Employee;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {

    private final EmployeeService employeeService;
    private final CompanyService commpanyService;

    public CompanyController(CompanyService companyService, EmployeeService employeeService) {
        this.commpanyService = companyService;
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Company> getCompanies() {
        return commpanyService.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable Long id) {
        return commpanyService.getCompanyById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id) {
        return employeeService.findByCompanyId(id);
    }

    @GetMapping(params = {"page", "size"})
    public List<Company> getCompaniesByPagination(@RequestParam Integer page,
                                                  @RequestParam Integer size) {
        return commpanyService.getCompaniesByPagination(page, size);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company) {
        return commpanyService.create(company);
    }

    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return commpanyService.update(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable Long id) {
        commpanyService.remove(id);
    }
}
